<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder
{
    public function run()
    {

        $users = [
            [
                'name' => 'Tester',
                'email' => 'test@holonglobe.com',
                'password' => bcrypt('Test123'),
            ]
        ];

        foreach ($users as $user) {
            $userExists = DB::table('users')
                ->where('users.email', $user['email'])
                ->count();

            if ($userExists == 0) {
                DB::table('users')->insert($user);
                $this->command->info('New user ' . $user['name'] . ' was added!');
            }
        }
    }

}