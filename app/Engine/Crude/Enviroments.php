<?php

namespace App\Engine\Crude;

use Crude;
use CrudeListInterface;
use CrudeFromModelTrait;
use CrudeStoreInterface;
use CrudeUpdateInterface;
use CrudeWithValidationInterface;
use CrudeWithValidationTrait;

class Enviroments extends Crude implements
    CrudeListInterface,
    CrudeStoreInterface,
    CrudeUpdateInterface,
    CrudeWithValidationInterface
{

    use CrudeFromModelTrait;
    use CrudeWithValidationTrait;

    public function __construct()
    {
        $this->setModel(new \App\Engine\Models\Enviroments);
        $this->prepareCrudeSetup();
        $this->crudeSetup
			->setTitle(trans('titles.enviroments'))
			->setColumn(['id', 'name', 'url'])
			->setTrans(trans('enviroments.attributes'));
		$this->setValidationRules([
				'name' => 'required',
				'url' => 'required'
			]);
    }

    public function prepareQuery()
	{
    	return $this->model
        	->select(
        	'enviroments.id',
            'enviroments.name',
            'enviroments.url'
		);
	}

	public function getFileCustomActionPermission($model)
    {
    	return true;
    }

    public function getFileCustomAction($model)
    {
        $Handle = (new \App\Engine\Models\Enviroments)->DownloadLogs($model->name, $model->url);
        return $Handle;
    }

}