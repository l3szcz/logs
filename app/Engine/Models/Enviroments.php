<?php

namespace App\Engine\Models;

use Illuminate\Database\Eloquent\Model;
use Storage; 

class Enviroments extends Model
{

	private $Key = 'HGAPI_';

    protected $fillable = [
        'name',
        'url'
    ];

    public static function Curl($url)
    {
    	$url = $url . '/get-logs';
    	$token = ['token'	=>	sha1($this->Key.date('Y-m-d'))];

 		$ch = curl_init();  

		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_HEADER, false); 
		curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $token);    
 
		$output = curl_exec($ch);
 
		curl_close($ch);
    	return $output;
    }

    public static function DownloadLogs($name, $url)
    {
    	$Input = storage_path('logs/laravel_' . $name .'.log');
    	$Output = $this->Curl($url);
    	if( file_exists($Input) )
			Storage::delete($Input);

		$Save = Storage::put($input, $Output);
		return $Save;
    }
}
