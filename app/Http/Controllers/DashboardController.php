<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Auth;

class DashboardController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function dashboard()
    {
        return view('enviroments', [
            'crudeSetup' => [(new \App\Engine\Crude\Enviroments)->getCrudeSetupData()]
        ]);
    }
}